package com.microUserApp.microUser.user.application.mapper;

import com.microUserApp.microUser.user.application.dto.UserRequest;
import com.microUserApp.microUser.user.domain.model.User;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-10T18:29:05-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class UserRequestMapperImpl implements UserRequestMapper {

    @Override
    public UserRequest toUserRequest(User user) {
        if ( user == null ) {
            return null;
        }

        UserRequest userRequest = new UserRequest();

        userRequest.setId( user.getId() );
        userRequest.setName( user.getName() );
        userRequest.setLastName( user.getLastName() );
        userRequest.setIdentificationNumber( user.getIdentificationNumber() );
        userRequest.setPassword( user.getPassword() );
        userRequest.setStatus( user.getStatus() );
        userRequest.setDateOfBirth( user.getDateOfBirth() );
        userRequest.setPhone( user.getPhone() );
        userRequest.setEmail( user.getEmail() );
        userRequest.setIdRole( user.getIdRole() );

        return userRequest;
    }
}
