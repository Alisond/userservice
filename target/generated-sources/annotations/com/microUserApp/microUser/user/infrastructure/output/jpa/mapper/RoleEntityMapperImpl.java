package com.microUserApp.microUser.user.infrastructure.output.jpa.mapper;

import com.microUserApp.microUser.user.domain.model.Role;
import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.RoleEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-10T18:29:06-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class RoleEntityMapperImpl implements RoleEntityMapper {

    @Override
    public RoleEntity toEntity(Role role) {
        if ( role == null ) {
            return null;
        }

        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setId( role.getId() );
        roleEntity.setRole( role.getRole() );

        return roleEntity;
    }

    @Override
    public List<Role> toRoleList(List<RoleEntity> roleEntityList) {
        if ( roleEntityList == null ) {
            return null;
        }

        List<Role> list = new ArrayList<Role>( roleEntityList.size() );
        for ( RoleEntity roleEntity : roleEntityList ) {
            list.add( toRole( roleEntity ) );
        }

        return list;
    }
}
