package com.microUserApp.microUser.user.infrastructure.output.jpa.mapper;

import com.microUserApp.microUser.user.domain.model.User;
import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.UserEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-10T18:29:06-0500",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class UserEntityMapperImpl implements UserEntityMapper {

    @Override
    public UserEntity toEntity(User user) {
        if ( user == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setId( user.getId() );
        userEntity.setName( user.getName() );
        userEntity.setLastName( user.getLastName() );
        userEntity.setIdentificationNumber( user.getIdentificationNumber() );
        userEntity.setStatus( user.getStatus() );
        userEntity.setDateOfBirth( user.getDateOfBirth() );
        userEntity.setPhone( user.getPhone() );
        userEntity.setEmail( user.getEmail() );
        userEntity.setPassword( user.getPassword() );
        userEntity.setIdRole( user.getIdRole() );

        return userEntity;
    }

    @Override
    public User toUser(UserEntity userEntity) {
        if ( userEntity == null ) {
            return null;
        }

        User user = new User();

        user.setId( userEntity.getId() );
        user.setName( userEntity.getName() );
        user.setLastName( userEntity.getLastName() );
        user.setIdentificationNumber( userEntity.getIdentificationNumber() );
        user.setStatus( userEntity.getStatus() );
        user.setDateOfBirth( userEntity.getDateOfBirth() );
        user.setPhone( userEntity.getPhone() );
        user.setEmail( userEntity.getEmail() );
        user.setPassword( userEntity.getPassword() );
        user.setIdRole( userEntity.getIdRole() );

        return user;
    }

    @Override
    public List<User> toUserList(List<UserEntity> userEntityList) {
        if ( userEntityList == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( userEntityList.size() );
        for ( UserEntity userEntity : userEntityList ) {
            list.add( toUser( userEntity ) );
        }

        return list;
    }
}
