package com.microUserApp.microUser.user.infrastructure.input.rest;

import com.microUserApp.microUser.user.application.dto.UserRequest;
import com.microUserApp.microUser.user.application.dto.UserResponse;
import com.microUserApp.microUser.user.application.handler.IUserHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1",method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH})
public class UserRestController {
    protected static final Logger logger = LogManager.getLogger();
    private final IUserHandler userHandler;

    public UserRestController(IUserHandler userHandler) {
        this.userHandler = userHandler;
    }


    @GetMapping("/user/{userId}")
    public ResponseEntity<UserResponse> getUser(@PathVariable( name="userId")  Long userId){
        logger.info("ejecuté un get de userid!" + userId);
        return ResponseEntity.ok(userHandler.getUser(userId));
    }
    @PostMapping("/user")
    public ResponseEntity<Void> saveUser(@RequestBody UserRequest userRequest){
        System.out.println("UserRestController");
        System.out.println(userRequest.getIdRole());
        userHandler.saveUser(userRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>> getAllUsers(){
        return ResponseEntity.ok(userHandler.getAllUsers());
    }

    @PatchMapping("/user/disable/{userId}")
    public ResponseEntity<Void> disableUser(@PathVariable(name="userId")  Long userId){
        userHandler.disableUser(userId);
        return ResponseEntity.status(200).build();
    }

 //   @PatchMapping("/user/{id}")
   // public ResponseEntity<Void> disableUser(@PathVariable(name = "id") Long id) {
     //   userHandler.disableUser();

    //}

}
