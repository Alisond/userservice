package com.microUserApp.microUser.user.domain.api;

import com.microUserApp.microUser.user.domain.model.Role;

import java.util.List;

public interface IRoleServicePort {
    Role saveRole(Role role);
    List<Role> getAllRoles();
    Role getRole(Long idRole);
    void updateRole(Role role);
    void deleteRole(Long idRole);
}
