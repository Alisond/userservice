package com.microUserApp.microUser.user.application.mapper;

import com.microUserApp.microUser.user.application.dto.RoleDTO;
import com.microUserApp.microUser.user.application.dto.UserResponse;
import com.microUserApp.microUser.user.domain.model.Role;
import com.microUserApp.microUser.user.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        uses = {RoleDTOMapper.class}
)
public interface UserResponseMapper {
    RoleDTOMapper INSTANCE = Mappers.getMapper(RoleDTOMapper.class);

    default UserResponse toResponse(User user, RoleDTO roleDTO){
        UserResponse userResponse = new UserResponse();
        userResponse.setName(user.getName());
        userResponse.setIdentificationNumber(user.getIdentificationNumber());
        userResponse.setEmail(user.getEmail());
        userResponse.setStatus(user.getStatus());
        userResponse.setLastName(user.getLastName());
        userResponse.setDateOfBirth(user.getDateOfBirth());
        userResponse.setPhone(user.getPhone());
        userResponse.setRole(roleDTO);
        return userResponse;
    };

    default List<UserResponse> toResponseList(List<User> userList, List<Role> roleList){
        return userList.stream()
                .map(user -> {
                    UserResponse userResponse = new UserResponse();
                    userResponse.setName(user.getName());
                    userResponse.setEmail(user.getEmail());
                    userResponse.setPhone(user.getPhone());
                    userResponse.setStatus(user.getStatus());
                    userResponse.setDateOfBirth(user.getDateOfBirth());
                    userResponse.setLastName(user.getLastName());
                    userResponse.setIdentificationNumber(user.getIdentificationNumber());
                    userResponse.setRole(INSTANCE.toDto(roleList.stream().filter(role-> role.getId().equals(user.getIdRole())).findFirst().orElse(null)));
                    return userResponse;
                }).toList();
    }
}
