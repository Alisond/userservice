package com.microUserApp.microUser.user.application.handler;

import com.microUserApp.microUser.user.application.dto.UserRequest;
import com.microUserApp.microUser.user.application.dto.UserResponse;

import java.util.List;

public interface IUserHandler {
    void saveUser(UserRequest userRequest);
    List<UserResponse> getAllUsers();
    UserResponse getUser(Long userId);
    void updateUser(UserRequest user);
    void disableUser(Long userId);
}
