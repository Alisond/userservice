package com.microUserApp.microUser.user.infrastructure.output.jpa.adapter;

import com.microUserApp.microUser.user.domain.model.User;
import com.microUserApp.microUser.user.domain.spi.IUserPersistencePort;
import com.microUserApp.microUser.user.infrastructure.exception.NoDataFoundException;
import com.microUserApp.microUser.user.infrastructure.exception.UserNotFoundException;
import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.UserEntity;
import com.microUserApp.microUser.user.infrastructure.output.jpa.mapper.UserEntityMapper;
import com.microUserApp.microUser.user.infrastructure.output.jpa.repository.IUserRepository;

import java.util.List;

public class UserJpaAdapter implements IUserPersistencePort {
    private final IUserRepository userRepository;
    private final UserEntityMapper userEntityMapper;

    public UserJpaAdapter(IUserRepository userRepository, UserEntityMapper userEntityMapper) {
        this.userRepository = userRepository;
        this.userEntityMapper = userEntityMapper;
    }

    @Override
    public void saveUser(User user) {
        System.out.println("UserJpaAdapter");
        System.out.println(user.getIdRole());
        userRepository.save(userEntityMapper.toEntity(user));
    }

    @Override
    public List<User> getAllUsers() {
        List<UserEntity> userEntityList = userRepository.findAll();
        if(userEntityList.isEmpty()){
            throw new NoDataFoundException();
        }
        return userEntityMapper.toUserList(userEntityList);
    }

    @Override
    public User getUser(Long userId) {

        return userEntityMapper.toUser(userRepository.findById(userId).orElseThrow(UserNotFoundException::new));
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(userEntityMapper.toEntity(user));
    }

    @Override
    public void disableUser(User user) {
        userRepository.save(userEntityMapper.toEntity(user));
    }
}
