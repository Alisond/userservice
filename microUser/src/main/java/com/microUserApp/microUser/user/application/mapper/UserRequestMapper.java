package com.microUserApp.microUser.user.application.mapper;


import com.microUserApp.microUser.user.application.dto.UserRequest;
import com.microUserApp.microUser.user.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface UserRequestMapper {

    default User toUser(UserRequest userRequest){
        if ( userRequest == null ) {
            return null;
        }

        User user = new User();
        user.setName( userRequest.getName() );
        user.setLastName( userRequest.getLastName() );
        user.setIdentificationNumber( userRequest.getIdentificationNumber() );
        user.setStatus( userRequest.getStatus() );
        user.setDateOfBirth( userRequest.getDateOfBirth() );
        user.setPhone( userRequest.getPhone() );
        user.setEmail( userRequest.getEmail() );
        user.setIdRole(userRequest.getIdRole());
        user.setPassword(userRequest.getPassword());

        return user;
    };

    UserRequest toUserRequest(User user);
    default Long toRole(UserRequest userRequest){
        Long role = userRequest.getIdRole();
        return role;
    };
}
