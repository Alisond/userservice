package com.microUserApp.microUser.user.infrastructure.output.jpa.repository;

import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRoleRepository extends JpaRepository<RoleEntity,Long> {

}
