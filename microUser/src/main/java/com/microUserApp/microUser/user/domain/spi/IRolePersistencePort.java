package com.microUserApp.microUser.user.domain.spi;

import com.microUserApp.microUser.user.domain.model.Role;

import java.util.List;

public interface IRolePersistencePort {
    void saveRole(Role role);
    List<Role> getAllRoles();
    Role getRole(Long idRole);
    void updateRole(Role role);
    void deleteRole(Long idRole);
}
