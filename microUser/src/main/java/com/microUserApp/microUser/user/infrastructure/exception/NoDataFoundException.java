package com.microUserApp.microUser.user.infrastructure.exception;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException() {
        super();
    }
}
