package com.microUserApp.microUser.user.infrastructure.output.jpa.mapper;

import com.microUserApp.microUser.user.domain.model.Role;
import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.RoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface RoleEntityMapper {
    RoleEntity toEntity(Role role);
    default Role toRole(RoleEntity roleEntity){
        return new Role(roleEntity.getId(),roleEntity.getRole());
    };
    List<Role> toRoleList(List<RoleEntity> roleEntityList);
}
