package com.microUserApp.microUser.user.infrastructure.output.jpa.repository;

import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<UserEntity,Long> {
}
