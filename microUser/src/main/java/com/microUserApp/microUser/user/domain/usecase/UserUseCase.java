package com.microUserApp.microUser.user.domain.usecase;

import com.microUserApp.microUser.user.domain.api.IUserServicePort;
import com.microUserApp.microUser.user.domain.model.User;
import com.microUserApp.microUser.user.domain.spi.IUserPersistencePort;

import java.util.List;

public class UserUseCase implements IUserServicePort {

    IUserPersistencePort userPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }


    @Override
    public void saveUser(User user) {
        userPersistencePort.saveUser(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userPersistencePort.getAllUsers();
    }

    @Override
    public User getUser(Long userId) {
        return userPersistencePort.getUser(userId);
    }

    @Override
    public void updateUser(User user) {
        userPersistencePort.saveUser(user);
    }

    @Override
    public void disableUser(User user) {
        userPersistencePort.disableUser(user);
    }
}
