package com.microUserApp.microUser.user.infrastructure.exception;

public class RoleNotFoundException extends RuntimeException{

    public RoleNotFoundException() {
        super();
    }
}
