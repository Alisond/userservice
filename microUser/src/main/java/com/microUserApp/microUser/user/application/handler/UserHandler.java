package com.microUserApp.microUser.user.application.handler;

import com.microUserApp.microUser.user.application.dto.UserRequest;
import com.microUserApp.microUser.user.application.dto.UserResponse;
import com.microUserApp.microUser.user.application.mapper.RoleDTOMapper;
import com.microUserApp.microUser.user.application.mapper.UserRequestMapper;
import com.microUserApp.microUser.user.application.mapper.UserResponseMapper;
import com.microUserApp.microUser.user.domain.api.IRoleServicePort;
import com.microUserApp.microUser.user.domain.api.IUserServicePort;
import com.microUserApp.microUser.user.domain.model.User;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class UserHandler implements IUserHandler{

    private final IUserServicePort userServicePort;
    private final IRoleServicePort roleServicePort;
    private final UserRequestMapper userRequestMapper;
    private final UserResponseMapper userResponseMapper;
    private final RoleDTOMapper roleDTOMapper;

    public UserHandler(IUserServicePort userServicePort, IRoleServicePort roleServicePort, UserRequestMapper userRequestMapper, UserResponseMapper userResponseMapper, RoleDTOMapper roleDTOMapper) {
        this.userServicePort = userServicePort;
        this.roleServicePort = roleServicePort;
        this.userRequestMapper = userRequestMapper;
        this.userResponseMapper = userResponseMapper;
        this.roleDTOMapper = roleDTOMapper;
    }

    @Override
    public void saveUser(UserRequest userRequest) {
        User user = userRequestMapper.toUser(userRequest);
        userServicePort.saveUser(user);
    }

    @Override
    public List<UserResponse> getAllUsers() {
        return userResponseMapper.toResponseList(userServicePort.getAllUsers(),roleServicePort.getAllRoles());
    }

    @Override
    public UserResponse getUser(Long userId) {
        User user = userServicePort.getUser(userId);

        return userResponseMapper.toResponse(user,roleDTOMapper.toDto(roleServicePort.getRole(user.getIdRole())));
    }

    @Override
    public void updateUser(UserRequest userRequest) {
        User oldUser = userServicePort.getUser(userRequest.getId());
        User newUser = userRequestMapper.toUser(userRequest);
        newUser.setId(oldUser.getId());
        newUser.setIdRole(oldUser.getId());
        userServicePort.updateUser(newUser);

    }

    @Override
    public void disableUser(Long userId) {
        User userToDisable = userServicePort.getUser(userId);

        userToDisable.setStatus(false);
        userServicePort.updateUser(userToDisable);

    }
}
