package com.microUserApp.microUser.user.infrastructure.exception;

public class UserNotFoundException extends RuntimeException{

    public UserNotFoundException() {
        super();
    }
}
