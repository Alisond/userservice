package com.microUserApp.microUser.user.infrastructure.configuration;

import com.microUserApp.microUser.user.domain.api.IRoleServicePort;
import com.microUserApp.microUser.user.domain.api.IUserServicePort;
import com.microUserApp.microUser.user.domain.spi.IRolePersistencePort;
import com.microUserApp.microUser.user.domain.spi.IUserPersistencePort;
import com.microUserApp.microUser.user.domain.usecase.RoleUseCase;
import com.microUserApp.microUser.user.domain.usecase.UserUseCase;
import com.microUserApp.microUser.user.infrastructure.output.jpa.adapter.RoleJpaAdapter;
import com.microUserApp.microUser.user.infrastructure.output.jpa.adapter.UserJpaAdapter;
import com.microUserApp.microUser.user.infrastructure.output.jpa.mapper.RoleEntityMapper;
import com.microUserApp.microUser.user.infrastructure.output.jpa.mapper.UserEntityMapper;
import com.microUserApp.microUser.user.infrastructure.output.jpa.repository.IRoleRepository;
import com.microUserApp.microUser.user.infrastructure.output.jpa.repository.IUserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {
    private final IUserRepository userRepository;
    private final UserEntityMapper userEntityMapper;
    private final IRoleRepository roleRepository;
    private final RoleEntityMapper roleEntityMapper;

    public BeanConfiguration(IUserRepository userRepository, UserEntityMapper userEntityMapper, IRoleRepository roleRepository, RoleEntityMapper roleEntityMapper) {
        this.userRepository = userRepository;
        this.userEntityMapper = userEntityMapper;
        this.roleRepository = roleRepository;
        this.roleEntityMapper = roleEntityMapper;
    }


    @Bean
    public IUserPersistencePort userPersistencePort(){
        return new UserJpaAdapter(userRepository,userEntityMapper);
    }
    @Bean
    public IUserServicePort userServicePort(){
        return new UserUseCase(userPersistencePort());
    }

    @Bean
    public IRolePersistencePort rolePersistencePort(){
        return new RoleJpaAdapter(roleRepository,roleEntityMapper);
    }
    @Bean
    public IRoleServicePort roleServicePort(){
        return new RoleUseCase(rolePersistencePort());
    }
}
