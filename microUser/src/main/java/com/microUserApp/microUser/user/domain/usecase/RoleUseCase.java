package com.microUserApp.microUser.user.domain.usecase;

import com.microUserApp.microUser.user.domain.api.IRoleServicePort;
import com.microUserApp.microUser.user.domain.model.Role;
import com.microUserApp.microUser.user.domain.spi.IRolePersistencePort;

import java.util.List;

public class RoleUseCase implements IRoleServicePort {

    IRolePersistencePort rolePersistencePort;

    public RoleUseCase(IRolePersistencePort rolePersistencePort) {
        this.rolePersistencePort = rolePersistencePort;
    }

    @Override
    public Role saveRole(Role role) {
        rolePersistencePort.saveRole(role);
        return role;
    }

    @Override
    public List<Role> getAllRoles() {
        return rolePersistencePort.getAllRoles();
    }

    @Override
    public Role getRole(Long idRole) {
        return rolePersistencePort.getRole(idRole);
    }

    @Override
    public void updateRole(Role role) {
        rolePersistencePort.updateRole(role);
    }

    @Override
    public void deleteRole(Long idRole) {
        rolePersistencePort.deleteRole(idRole);
    }
}
