package com.microUserApp.microUser.user.domain.spi;

import com.microUserApp.microUser.user.domain.model.User;

import java.util.List;

public interface IUserPersistencePort {
    void saveUser(User user);
    List<User> getAllUsers();
    User getUser(Long userId);
    void updateUser(User user);
    void disableUser(User user);
}
