package com.microUserApp.microUser.user.infrastructure.exception;

public class RoleAlreadyExistsException extends RuntimeException{

    public RoleAlreadyExistsException() {
        super();
    }
}
