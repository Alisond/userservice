package com.microUserApp.microUser.user.domain.api;

import com.microUserApp.microUser.user.domain.model.User;

import java.util.List;

public interface IUserServicePort {
    void saveUser(User user);
    List<User> getAllUsers();
    User getUser(Long userId);
    void updateUser(User user);
    void disableUser(User user);
}
