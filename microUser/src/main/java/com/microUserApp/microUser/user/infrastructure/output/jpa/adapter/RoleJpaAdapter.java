package com.microUserApp.microUser.user.infrastructure.output.jpa.adapter;

import com.microUserApp.microUser.user.domain.model.Role;
import com.microUserApp.microUser.user.domain.spi.IRolePersistencePort;
import com.microUserApp.microUser.user.infrastructure.exception.NoDataFoundException;
import com.microUserApp.microUser.user.infrastructure.exception.RoleAlreadyExistsException;
import com.microUserApp.microUser.user.infrastructure.exception.UserNotFoundException;
import com.microUserApp.microUser.user.infrastructure.output.jpa.entity.RoleEntity;
import com.microUserApp.microUser.user.infrastructure.output.jpa.mapper.RoleEntityMapper;
import com.microUserApp.microUser.user.infrastructure.output.jpa.repository.IRoleRepository;

import java.util.List;
import java.util.Optional;

public class RoleJpaAdapter implements IRolePersistencePort {

    private final IRoleRepository roleRepository;
    private final RoleEntityMapper roleEntityMapper;
    public RoleJpaAdapter(IRoleRepository roleRepository, RoleEntityMapper roleEntityMapper) {
        this.roleRepository = roleRepository;
        this.roleEntityMapper = roleEntityMapper;
    }
    @Override
    public void saveRole(Role role) {
        if (roleRepository.findById(role.getId()).isPresent()){
            throw new RoleAlreadyExistsException();
        }
        roleRepository.save(roleEntityMapper.toEntity(role));
    }

    @Override
    public List<Role> getAllRoles() {
        List<RoleEntity> roleEntityList = roleRepository.findAll();
        if(roleEntityList.isEmpty()){
            throw new NoDataFoundException();
        }
        return roleEntityMapper.toRoleList(roleEntityList);
    }

    @Override
    public Role getRole(Long idRole) {
        System.out.println("Role JPAapadpter");
        System.out.println(idRole);
        Optional<RoleEntity> role = roleRepository.findById(idRole);
        System.out.println(role.get().getRole());
        Role role1 = roleEntityMapper.toRole(roleRepository.findById(idRole).orElseThrow(UserNotFoundException::new));
        System.out.println(role1.getRole());

        return roleEntityMapper.toRole(roleRepository.findById(idRole).orElseThrow(UserNotFoundException::new));
    }

    @Override
    public void updateRole(Role role) {
        roleRepository.save(roleEntityMapper.toEntity(role));

    }

    @Override
    public void deleteRole(Long idRole) {
        roleRepository.deleteById(idRole);
    }
}
