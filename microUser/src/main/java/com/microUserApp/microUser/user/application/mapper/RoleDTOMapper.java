package com.microUserApp.microUser.user.application.mapper;

import com.microUserApp.microUser.user.application.dto.RoleDTO;
import com.microUserApp.microUser.user.domain.model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface RoleDTOMapper {
    default RoleDTO toDto(Role role){
        return new RoleDTO(role.getRole());
    };
}
